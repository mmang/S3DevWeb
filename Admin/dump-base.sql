-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Sep 08, 2010 at 06:11 PM
-- Server version: 5.0.41
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `palais`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `categories`
-- 

CREATE TABLE `categories` (
  `idCat` int(11) NOT NULL auto_increment,
  `nomCat` varchar(20) collate utf8_unicode_ci NOT NULL default '',
  `parent` int(11) NOT NULL default '0',
  PRIMARY KEY  (`idCat`),
  UNIQUE KEY `categorie` (`nomCat`,`parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

-- 
-- Dumping data for table `categories`
-- 

INSERT INTO `categories` VALUES (1, 'douces', 0);
INSERT INTO `categories` VALUES (2, 'dures', 0);
INSERT INTO `categories` VALUES (3, 'chimiques', 0);
INSERT INTO `categories` VALUES (4, 'héro', 2);
INSERT INTO `categories` VALUES (5, 'coke', 2);
INSERT INTO `categories` VALUES (9, 'shit', 1);
INSERT INTO `categories` VALUES (10, 'beuh', 1);
INSERT INTO `categories` VALUES (18, 'marocain', 9);

-- --------------------------------------------------------

-- 
-- Table structure for table `produits`
-- 

CREATE TABLE `produits` (
  `id` int(11) NOT NULL auto_increment,
  `nom` char(50) collate utf8_unicode_ci NOT NULL default '',
  `idcategorie` mediumint(11) NOT NULL,
  `provenance` char(50) collate utf8_unicode_ci NOT NULL default '',
  `qualite` char(50) collate utf8_unicode_ci NOT NULL default '',
  `description` varchar(254) collate utf8_unicode_ci NOT NULL default '',
  `prix` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

-- 
-- Dumping data for table `produits`
-- 

INSERT INTO `produits` VALUES (1, 'spécial', 18, 'Maroc : région du Rif', '1ère passe', 'haschisch fort et qui pique légèrement la gorge, de couleur sable ou brun clair assez compact et poudreux.', 18);
INSERT INTO `produits` VALUES (2, 'double zéro', 18, 'Maroc : région du Rif', '2ème passe', 'haschisch fort et qui pique légèrement la gorge, de couleur sable ou brun clair assez compact et poudreux.', 15);
INSERT INTO `produits` VALUES (3, 'resina', 18, 'Maroc : région du Rif', '3ème passe (mélangé)', 'haschisch fort et qui pique légèrement la gorge, de couleur sable ou brun clair assez compact et poudreux.', 10);
INSERT INTO `produits` VALUES (4, 'afghan', 9, 'Afghanistan : est du pays', 'moyen', 'haschisch envoûtant et doux, de couleur noire ou réglisse', 15);
INSERT INTO `produits` VALUES (5, 'pakistanais', 9, 'Pakistan : région de Peshawar', 'moyen', 'haschisch envoûtant et doux, de couleur noire ou réglisse', 15);
INSERT INTO `produits` VALUES (6, 'népalais', 9, 'Népal : contreforts de l''Himalaya', 'top', 'haschisch envoûtant et doux, de couleur noire ou réglisse', 20);
INSERT INTO `produits` VALUES (7, 'libanais', 9, 'Liban : monts anti-liban', 'top', 'haschisch de couleur sable-rouge', 15);
INSERT INTO `produits` VALUES (8, 'hollandais', 9, 'Pays-Bas', 'moyen', 'haschisch produit à partir de cultures sous serre', 10);
INSERT INTO `produits` VALUES (9, 'tijuana', 5, 'rupture de stock', '', 'notre cargo s''est fait arraisonner par la douane française au large des Antilles !', 0);
INSERT INTO `produits` VALUES (10, 'waziristane', 4, 'rupture de stock', '', 'notre labo a été détruit par les troupes de l''OTAN !', 0);
INSERT INTO `produits` VALUES (11, 'panshirienne', 4, 'arrêt de la fabrication', '', 'un problème a été détecté sur nos chaînes de conditionnement', 0);
INSERT INTO `produits` VALUES (12, 'bové', 10, 'rupture de stock', '', 'la plantation a été contaminée par du chanvre OGM', 0);
INSERT INTO `produits` VALUES (13, 'locale', 10, 'rupture de stock', '', 'attente de la prochaine récolte !', 0);
INSERT INTO `produits` VALUES (14, 'skunk', 10, 'rupture de stock', '', 'Le livreur s''est fait arrêter par la douane volante', 0);
INSERT INTO `produits` VALUES (15, 'pakalolo', 10, '', 'rupture de stock', 'notre cargo s''est fait arraisoner par la douane française !', 12);

-- --------------------------------------------------------

-- 
-- Table structure for table `utilisateurs`
-- 

CREATE TABLE `utilisateurs` (
  `nom` varchar(100) collate utf8_unicode_ci NOT NULL,
  `prenom` varchar(100) collate utf8_unicode_ci NOT NULL,
  `login` varchar(20) collate utf8_unicode_ci NOT NULL,
  `passe` varchar(32) collate utf8_unicode_ci NOT NULL,
  `courriel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `question` varchar(100) collate utf8_unicode_ci NOT NULL,
  `reponse` varchar(100) collate utf8_unicode_ci NOT NULL,
  `admin` tinyint(10) NOT NULL default '0',
  `id` int(10) NOT NULL auto_increment,
  PRIMARY KEY  (`id`),
  KEY `nom` (`nom`,`prenom`,`login`,`courriel`),
  KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `utilisateurs`
-- 

INSERT INTO `utilisateurs` (`nom`, `prenom`, `login`, `passe`, `courriel`, `question`, `reponse`, `admin`, `id`) VALUES
('pignon', 'françois', 'pignon', '2f0082dcf71133f2d6237b3b4f6bf72a', 'francois@pignon.net', 'pignon', 'pignon', 0, 2),
('toto', 'toto', 'toto', 'f71dbe52628a3f83a77ab494817525c6', 'toto', 'toto', 'toto', 0, 3),
('ky', 'jun', 'junky', '82546e4c24a391f283de2c8dcbfaea25', 'a@b.cd', 'junky', 'junky', 0, 5),
('admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.fr', 'admin', 'admin', 1, 6);
