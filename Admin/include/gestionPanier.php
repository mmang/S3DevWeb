<?php
	if (!isset($_SESSION[SESSION_TOTAL_PRIX])) {
		$_SESSION[SESSION_TOTAL_PRIX] = 0;
	}
	if (!isset($_SESSION[SESSION_TOTAL_QUANTITE])) {
		$_SESSION[SESSION_TOTAL_QUANTITE] = 0;
	}


	function ajoutePanier($produit, $quantite, $prix) {
		$panier = $_SESSION[SESSION_PANIER];
		if (!empty($panier[$produit])) {
			$panier[$produit] += $quantite;
		}
		else {
			$panier[$produit] = $quantite;
		}
		$_SESSION[SESSION_PANIER] = $panier;
		$_SESSION[SESSION_TOTAL_PRIX] += $prix*$quantite;
		$_SESSION[SESSION_TOTAL_QUANTITE] += $quantite;
	}

	function initPanier($produit, $quantite, $prix) {
		$panier = $_SESSION[SESSION_PANIER];
		
		$totalQuantite = 0;
		$totalPrix = 0;
		if (!empty($panier[$produit])) {
			$totalQuantite = -$panier[$produit];
			$totalPrix = $totalQuantite * $prix;
		}
		
		$panier[$produit] = $quantite;
		$_SESSION[SESSION_PANIER] = $panier;
		$_SESSION[SESSION_TOTAL_PRIX] += $totalPrix + $prix*$quantite;
		$_SESSION[SESSION_TOTAL_QUANTITE] += $totalQuantite + $quantite;
	}
	
	function supprimePanier($produit, $prix) {
		$panier = $_SESSION[SESSION_PANIER];
		
		if (!empty($panier[$produit])) {
			$quantite = $panier[$produit];
			$totalPrix = $quantite * $prix;
			$_SESSION[SESSION_TOTAL_PRIX] -= $totalPrix;
			$_SESSION[SESSION_TOTAL_QUANTITE] -= $quantite;
		}
		unset($panier[$produit]);
		$_SESSION[SESSION_PANIER] = $panier;
	}
?>
