<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 enregistrement BD" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, enregistrement avec BD" />
	<title>Formulaire de login</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Formulaire de login</h2>
	<hr />
</div>
<div id="centre">

<p>Si vous n'êtes pas encore logué, veuillez vous <a href="senregistrer.php">enregistrer</a>.</p>

<!-- login standard -->
<form action="login.php" method="post">
<fieldset>
	<legend>Login</legend>
<table class="centre" summary="formulaire de login" width="70%">
<tbody>
 <tr>
  <td><label for="Prénom" accesskey="l"><span class="accesskey">L</span>ogin :</label></td>
  <td><input type="text" id="Login" name="Login" /></td>
 </tr>
 <tr>
  <td><label for="Passe" accesskey="m"><span class="accesskey">M</span>ot de passe :</label></td>
  <td><input type="password" id="Passe" name="Passe" /></td>
 </tr>
 <tr>
  <td></td>
  <td><input type="submit" id="Loguer" name="Loguer" value="Se loguer" /></td>
 </tr>
</tbody>
</table>
</fieldset>
</form>

<!-- oubli de mot de passe -->
<p>En cas d'oubli de mot de passe, une question vous sera posée :</p>
<fieldset>
	<legend>Question</legend>
<form action="login.php" method="post">
<table class="centre" summary="formulaire de question" width="70%">
<tbody>
 <tr>
  <td><label for="Prénom" accesskey="l"><span class="accesskey">L</span>ogin :</label></td>
  <td><input type="text" id="Login" name="Login" /></td>
 </tr>
 <tr>
  <td><label for="Courriel" accesskey="c"><span class="accesskey">C</span>ourriel :</label></td>
  <td><input type="text" id="Courriel" name="Courriel" /></td>
 </tr>
 <tr>
  <td></td>
  <td><input type="submit" id="Question" name="Question" value="Question" /></td>
 </tr>
</tbody>
</table>
</fieldset>
</form>

<!-- oubli de login -->
<p>En cas d'oubli de login, il vous sera envoyé par courriel :</p>
<fieldset>
	<legend>Courriel</legend>
<form action="login.php" method="post">
<table class="centre" summary="formulaire de courriel" width="70%">
<tbody>
 <tr>
  <td><label for="Courriel" accesskey="c"><span class="accesskey">C</span>ourriel :</label></td>
  <td><input type="text" id="Courriel" name="Courriel" /></td>
 </tr>
 <tr>
  <td></td>
  <td><input type="submit" id="Envoi" name="Envoi" value="Envoi" /></td>
 </tr>
</tbody>
</table>
</fieldset>
</form>

</div>

<?php include(RACINE_SITE.'include/piedDePage.php');?>
