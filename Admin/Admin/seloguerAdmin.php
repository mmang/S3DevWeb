<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 login admin BD" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1, 
		exemple de script PHP, login admin avec BD" />
	<title>Formulaire de login administrateur</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2 class="admin">Login administrateur</h2>
	<hr />
</div>
<div id="centre">


<!-- login standard -->
<form action="loginAdmin.php" method="post">
<fieldset>
	<legend>Login administrateur</legend>
<table class="centre" summary="formulaire de login" width="70%">
<tbody>
 <tr>
  <td><label for="Prénom" accesskey="l"><span class="accesskey">L</span>ogin :</label></td>
  <td><input type="text" id="Login" name="Login" /></td>
 </tr>
 <tr>
  <td><label for="Passe" accesskey="m"><span class="accesskey">M</span>ot de passe :</label></td>
  <td><input type="password" id="Passe" name="Passe" /></td>
 </tr>
 <tr>
  <td></td>
  <td><input type="submit" id="Loguer" name="Loguer" value="Se loguer" /></td>
 </tr>
</tbody>
</table>
</fieldset>
</form>

</div>

<div id="piedDePage">
<p class="copyright">Copyright © Huggy Les Bons Tuyaux, tous droits réservés</p>
</div>
</body>
</html>
