<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 panier, sessions" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, panier avec sessions" />
	<title>Enregistrement utilisateur</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2 class="admin">Login administrateur</h2>
	<p> <a href="<?php echo RACINE_WEB;?>Pages/afficheCat.php">Catalogue</a></p>
	<hr />
</div>

<div id="partieCentrale">
<?php
	require_once(RACINE_SITE . 'include/connexion.php');

	// Login de base
	if (isset($_POST["Loguer"]) &&
		!empty($_POST["Login"]) &&
		!empty($_POST["Passe"])) {
		$passeCrypte = md5($_POST["Passe"]);
		$requete = 'SELECT admin, passe FROM utilisateurs WHERE login = \'' . $_POST["Login"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$utilisateur = mysqli_fetch_assoc($resultat);
			if (!empty($utilisateur)) {
				if (strcmp($utilisateur['passe'],$passeCrypte) == 0) {
					if (!empty($utilisateur['admin'])) {
						$_SESSION[SESSION_ADMIN] = $_POST["Login"];
						echo '<p>Administrateur ', $_POST["Login"],' logué</p>';
					}
					else {
						echo '<p>Erreur, utilisateur non admin !</p>';
					}
				}
				else {
					echo '<p>Erreur de mot de passe !</p>';
				}
			}
			else {
				echo '<p>Erreur, utilisateur inexistant !</p>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($CONNEXION);
		}
	}

	mysqli_close($CONNEXION);
?>
</div>
<div id="piedDePage">
<p class="copyright">Copyright © Huggy Les Bons Tuyaux, tous droits réservés</p>
</div>

</body>
</html>
