<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 déconnexion" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, déconnexion" />
	<title>Navigation dans un catalogue</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Palais de la dope !</h1>
	<h2>Au revoir</h2>
</div>

<div>
<?php
	if (!empty($_SESSION)) {
		foreach ($_SESSION as $cle => $variable) {
			unset($_SESSION[$cle]);
		}
	}
?>
<p style="text-align:center;">Utilisateur déconnecté</p>
<p style="text-align:center;"><a href="afficheCat.php">Retour à l'accueil</a></p>
</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
