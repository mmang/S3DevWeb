<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 catalogue" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, catalogue avec BD" />
	<title>Navigation dans un catalogue</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<p style="text-align:right">
<?php
	if (isset($_SESSION[SESSION_LOGIN])) {
		echo $_SESSION[SESSION_LOGIN];
		echo ' <a href="', RACINE_WEB,'Pages/deconnexion.php"> déconnexion</a>';
	}
	else {
		echo '<a href="seloguerCookie.php">login</a>';
	}
?>
	</p>
	<h1>Bienvenue au palais de la dope !</h1>
	<h3>Choisissez votre drogue : </h3>
	<hr />
</div>
<p> <a href="afficheCat.php">Catalogue</a></p>
<?php

	require_once(RACINE_SITE . 'include/connexion.php');
	require_once(RACINE_SITE . 'include/gestionPanier.php');

	if (!empty($_REQUEST["Produit"])) {

		if (!empty($_POST["Quantite"]) && !empty($_POST["Prix"])) {
			ajoutePanier($_POST["Produit"],$_POST["Quantite"],$_POST["Prix"]);
		}
	}
	if (!isset($_SESSION[SESSION_TOTAL_PRIX])) {
		$_SESSION[SESSION_TOTAL_PRIX] = 0;
	}
?>
<p style="text-align:right;">
		Total : <?php echo $_SESSION[SESSION_TOTAL_PRIX]; ?> €
		<a href="affichePanier.php">panier</a></p>
	<hr />
</div>

<div id="partieCentrale">
<?php

	require_once(RACINE_SITE . 'include/connexion.php');
	$categorie = 0;
	if (!empty($_REQUEST['Categorie'])) {
		$categorie = $_REQUEST['Categorie'];
	}

	// affichage du parent
	$requete = 'SELECT nomCat, parent from categories where idCat = \'' . $categorie . '\';';
	$resultat = mysqli_query($CONNEXION,$requete);
	if (!empty($resultat)) {
		$maCat = mysqli_fetch_assoc($resultat);
		if (!empty($maCat)) {
			echo '<p> <a href="afficheCat.php?Categorie=',$maCat['parent'],'">&lt;&lt;</a> <strong>',$maCat['nomCat'],'</strong></p>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}

	// affichage des sous-catégories
		echo '<ul>';
	$requete = 'SELECT nomCat, idCat from categories where parent = \'' . $categorie . '\';';
	$resultat = mysqli_query($CONNEXION,$requete);
	if (!empty($resultat)) {
		while ($maCat = mysqli_fetch_assoc($resultat)) {
			echo '<li> <a href="afficheCat.php?Categorie=',$maCat['idCat'],'">',$maCat['nomCat'],'</a></li>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}

	// affichage des produits
	$requete = 'SELECT nom, id from produits where idcategorie = \'' . $categorie . '\';';
	$resultat = mysqli_query($CONNEXION,$requete);
	if (!empty($resultat)) {
		while ($monProduit = mysqli_fetch_assoc($resultat)) {
			echo '<li> <a href="afficheProduit.php?Produit=',$monProduit['id'],'">',$monProduit['nom'],'</a></li>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}
		echo '</ul>';

	mysqli_close($CONNEXION);
?>
</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
