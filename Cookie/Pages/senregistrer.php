<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 enregistrement BD" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, enregistrement avec BD" />
	<title>Formulaire d'enregistrement</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Formulaire d'enregistrement</h2>
	<hr />
</div>
<div id="centre">
<form
	action="enregistrement.php"
	method="post">
<table class="centre" summary="formulaire" width="70%">
<tbody>
 <tr>
  <td><label for="Nom" accesskey="n"><span class="accesskey">N</span>om :</label></td>
  <td><input type="text" id="Nom" name="Nom" /></td>
 </tr>
 <tr>
  <td><label for="Prénom" accesskey="p"><span class="accesskey">P</span>rénom :</label></td>
  <td><input type="text" id="Prenom" name="Prenom" /></td>
 </tr>
 <tr>
  <td><label for="Prénom" accesskey="l"><span class="accesskey">L</span>ogin :</label></td>
  <td><input type="text" id="Login" name="Login" /></td>
 </tr>
 <tr>
  <td><label for="Passe" accesskey="m"><span class="accesskey">M</span>ot de passe :</label></td>
  <td><input type="password" id="Passe" name="Passe" /></td>
 </tr>
 <tr>
  <td><label for="PasseVerif" accesskey="f">Passe (con<span class="accesskey">f</span>irmation) :</label></td>
  <td><input type="password" id="PasseVerif" name="PasseVerif" /></td>
 </tr>
 <tr>
  <td><label for="Courriel" accesskey="c"><span class="accesskey">C</span>ourriel :</label></td>
  <td><input type="text" id="Courriel" name="Courriel" /></td>
 </tr>
 <tr>
  <td><label for="Question" accesskey="q"><span class="accesskey">Q</span>uestion :</label><br />
  	<span style="font-size:0.8em;">en cas d'oubli du mot de passe</span></td>
  <td><input type="text" id="Question" name="Question" /></td>
 </tr>
 <tr>
  <td><label for="Reponse" accesskey="r"><span class="accesskey">R</span>éponse :</label></td>
  <td><input type="text" id="Reponse" name="Reponse" /></td>
 </tr>
 <tr>
  <td></td>
  <td><input type="submit" id="Enregistrer" name="Enregistrer" value="S'enregistrer" /></td>
 </tr>
</tbody>
</table>
</form>
</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
