<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 panier" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, panier avec BD" />
	<title>Contenu du panier</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<?php

	require_once(RACINE_SITE . 'include/connexion.php');
	require_once(RACINE_SITE . 'include/gestionPanier.php');


	// suppression des produits sélectionnés
	if (!empty($_POST['Supprimer'])) {
		$ids = $_POST['SupprimeId'];
		foreach ($ids as $id) {
			$prix = $_POST['Prix'.$id];
			supprimePanier($id, $prix);
		}
	}

	// modification des produits sélectionnés
	if (!empty($_POST['Modifier'])) {
		$ids = $_POST['id'];
		foreach ($ids as $id) {
			$quantite = $_POST['Quantite'.$id];
			$prix = $_POST['Prix'.$id];
			initPanier($id, $quantite, $prix);
		}
	}
?>
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Contenu de votre panier </h2>
    <p> <a href="afficheCat.php">Catalogue</a></p>
		<p style="text-align:right;">
		Total : <?php echo $_SESSION[SESSION_TOTAL_PRIX]; ?> €
		<a href="affichePanier.php">panier</a></p>

</div>
<div id="centre">
<form action="affichePanier.php" method="post">
<table width="80%" style="margin-left:auto; margin-right:auto;" border="0" summary="contenu du panier">
	<thead>
		<tr>
			<th>Nom</th>
			<th>Prix</th>
			<th>Quantité</th>
			<th>Total</th>
			<th>Supprimer</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td style="text-align:right" colspan="6">
				<input type="submit" name="Modifier" value="Modifier" /> ou
				<input type="submit" name="Supprimer" value="Supprimer" />
			</td>
		</tr>
	</tfoot>
	<tbody>
<?php

	$panier = array();
	$total = 0;
	$quantites = 0;
	if (isset($_SESSION[SESSION_PANIER])) {
		$panier = $_SESSION[SESSION_PANIER];
	}

	foreach ($panier as $produit => $quantite) {

		// affichage du panier
		$requete = 'SELECT nom, prix FROM produits WHERE id = \'' . $produit . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$monProduit = mysqli_fetch_assoc($resultat);
			if (!empty($monProduit)) {
				$totalInt = $monProduit['prix'] * $quantite;
				$total += $totalInt;
				$quantites += $quantite;
				echo '<tr>
					<td style="text-align:center;">',$monProduit['nom'],'</td>
					<td style="text-align:center;">',$monProduit['prix'],' €/g</td>
					<td style="text-align:center;">
					<input type="hidden" name="id[]" value="',$produit,'" />
					<input type="text" name="Quantite',$produit,'" value="',$quantite,'" size="3" /></td>
					<td style="text-align:right;">', $totalInt,' €</td>
					<input type="hidden" name="Prix',$produit,'" value="',$monProduit['prix'],'" /></td>
					<td style="text-align:center;">
					<input type="checkbox" name="SupprimeId[]" value="',$produit,'" /></td>
					</tr>
					';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo 'Message de MySQL : ', mysqli_error($CONNEXION);
		}
	}
	echo '<tr><th>Total</th><td></td>',
		'<td style="text-align:center;">', $quantites, '</td>',
		'<td style="text-align:right;">', $total, ' €</td>',
		'<td></td></tr>';
	mysqli_close($CONNEXION);
?>
	</tbody>
</table>
</form>
</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
