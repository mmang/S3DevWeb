<?php
	require_once('../init.php');

	require_once(RACINE_SITE . 'include/connexion.php');
	$message = '';
	// Login de base
	if (isset($_POST["Loguer"]) &&
		!empty($_POST["Login"]) &&
		!empty($_POST["Passe"])) {
		$passeCrypte = md5($_POST["Passe"]);
		$requete = 'SELECT id, passe FROM utilisateurs WHERE login = \'' . $_POST["Login"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$utilisateur = mysqli_fetch_assoc($resultat);
			if (!empty($utilisateur)) {
				if (strcmp($utilisateur['passe'],$passeCrypte) == 0) {
					$_SESSION[SESSION_LOGIN] = $_POST["Login"];

					// envoi d'un cookie
					if (!empty($_POST["Cookie"])) {
						$valeur = $utilisateur['id'] . COOKIE_SEP . $passeCrypte;
						$expire = time() + (365 * 24 * 60 * 60);
						$message =  'Set cookie : '. COOKIE_NOM. ' : '. $valeur;
						setcookie (COOKIE_NOM, $valeur, $expire);
				}

					$message .= '<p>Utilisateur ' . $_POST['Login'] .' logué</p>';
				}
				else {
			$message .= '<p>Erreur de mot de passe !</p>';
				}
			}
			else {
				$message .= '<p>Erreur, utilisateur inexistant !</p>';
			}
		}
		else {
			$message .= "Erreur dans l'exécution de la requête.<br/>\n";
			$message .= "Message de MySQL : ". mysqli_error($CONNEXION);
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 panier, sessions" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, panier avec sessions" />
	<title>Enregistrement utilisateur</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Login utilisateur</h2>
	<p> <a href="afficheCat.php">Catalogue</a></p>
	<hr />
</div>

<div id="partieCentrale">
<?php
	echo '<p>',$message , '</p>';

	// Question
	if (isset($_POST["Question"]) &&
		!empty($_POST["Login"]) &&
		!empty($_POST["Courriel"])) {
		$requete = 'SELECT courriel, question FROM utilisateurs WHERE login = \'' . $_POST["Login"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$utilisateur = mysqli_fetch_assoc($resultat);
			if (!empty($utilisateur)) {
				if (strcmp($utilisateur['courriel'],$_POST["Courriel"]) == 0) {
					echo '<form action="loginCookie.php" method="post">
					<div>
					<fieldset>
						<legend>Question</legend>
						<p>Question : ', $utilisateur['question'],'</p>
						<label>Réponse : <input type="text" name="Reponse" /></label>
						<input type="hidden" name="Courriel" value="', $utilisateur['courriel'],'" />
						<p><input type="submit" name="Repondre" value="Repondre" /></p>
					</fieldset>
					</div>';
					//$_SESSION[SESSION_LOGIN] = $_REQUEST["Login"];
					//echo '<p>Utilisateur logué, <a href="changePasse.php">changez</a> votre mot de passe !</p>';
				}
				else {
					echo '<p>Erreur de courriel !</p>';
				}
			}
			else {
				echo '<p>Erreur, utilisateur inexistant !</p>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($CONNEXION);
		}
	}

	// Réponse
	if (isset($_POST["Reponse"]) &&
		!empty($_POST["Courriel"])) {
		$requete = 'SELECT reponse, login FROM utilisateurs WHERE courriel = \'' . $_POST["Courriel"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$utilisateur = mysqli_fetch_assoc($resultat);
			if (!empty($utilisateur)) {
				if (strcmp($utilisateur['reponse'],$_POST["Reponse"]) == 0) {
					$_SESSION[SESSION_LOGIN] = $utilisateur['login'];
					echo '<p>Utilisateur ', $utilisateur['login'],' logué,
					<a href="changePasse.php">changez</a> votre mot de passe !</p>';
				}
				else {
					echo '<p>Erreur de courriel !</p>';
				}
			}
			else {
				echo '<p>Erreur, utilisateur inexistant !</p>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($CONNEXION);
		}
	}

	// Courriel
	if (isset($_POST["Envoi"]) &&
		!empty($_POST["Courriel"])) {
		$requete = 'SELECT login FROM utilisateurs WHERE courriel = \'' . $_POST["Courriel"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$utilisateur = mysqli_fetch_assoc($resultat);
			if (!empty($utilisateur)) {
					// envoi de $utilisateur['login'] à $_REQUEST["Courriel"]
					echo '<p>Envoi de courriel à votre adresse</p>';
				}
			else {
				echo '<p>Erreur de courriel !</p>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($CONNEXION);
		}
	}

	mysqli_close($CONNEXION);
?>
</div>
<?php	include(RACINE_SITE.'include/piedDePage.php');?>
