<?php
	require_once('../init.php');

	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 enregistrement BD" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, enregistrement avec BD" />
	<title>Détection d'un cookie</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Détection d'un cookie</h2>
	<hr />
</div>
<div id="centre">

<p>Si vous n'êtes pas encore logué, veuillez vous <a href="senregistrer.php">enregistrer</a>.</p>

<?php
		// détection de la présence d'un cookie
        if (!empty($_COOKIE[COOKIE_NOM]))  {
        		// décodage du cookie
                $cookie_info = explode(COOKIE_SEP, $_COOKIE[COOKIE_NOM]);

                // pour débuguer
                echo "Un cookie a été récupéré<br/>Id: $cookie_info[0], Pass: $cookie_info[1]<br />";

        		// identification de l'utilisateur avec la BD
                require_once(RACINE_SITE.'include/connexion.php');
                $requete = 'SELECT id, login, passe FROM utilisateurs WHERE id = \''. $cookie_info[0] . '\';';

                $resultat = mysqli_query($CONNEXION,$requete);
                if ($resultat) {
                        // Login OK
                        $utilisateur = mysqli_fetch_assoc($resultat);
                        if (!empty($utilisateur)) {
                                if (strcmp($utilisateur['passe'],$cookie_info[1])==0) {
                                        $_SESSION[SESSION_LOGIN] = $utilisateur['login'];
                                        echo "Login OK<br/>\n";
                                }
                                else {
                                        echo "<p>Erreur de mot de passe, recommencez</p>\n";
                                }
                        }
                        else  {
                                echo '<p>Utilisateur inexistant</p>';
                        }
                }
        }
        else {
        	echo "<p>Pas de cookie trouvé, <a href='seloguerCookie.php'>identifiez-vous !</a></p>\n";
        }
?>
</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
