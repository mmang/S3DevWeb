<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 enregistrement BD" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, enregistrement avec BD" />
	<title>Formulaire de login</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Formulaire de login</h2>
	<hr />
</div>
<div id="centre">

<p>Si vous n'êtes pas encore logué, veuillez vous <a href="senregistrer.php">enregistrer</a>.</p>

<!-- login standard -->
<form action="loginCookie.php" method="post">
<fieldset>
	<legend>Login</legend>
<table class="centre" summary="formulaire de login" width="70%">
<tbody>
 <tr>
  <td><label for="Prénom" accesskey="l"><span class="accesskey">L</span>ogin :</label></td>
  <td><input type="text" id="Login" name="Login" /></td>
 </tr>
 <tr>
  <td><label for="Passe" accesskey="m"><span class="accesskey">M</span>ot de passe :</label></td>
  <td><input type="password" id="Passe" name="Passe" /></td>
 </tr>
  <tr>
  <td class="important"><label for="Cookie" accesskey="s">Se
  <span class="accesskey">s</span>ouvenir de moi avec un cookie :</label><br />
  Attention, n'importe qui pourra alors se connecter depuis mon ordinateur<br />
  En cas de problème, j'en serai alors responsable.</td>
  <td><input type="checkbox" id="Cookie" name="Cookie" /></td>
 </tr>

 <tr>
  <td></td>
  <td><input type="submit" id="Loguer" name="Loguer" value="Se loguer" /></td>
 </tr>
</tbody>
</table>
</fieldset>
</form>

</div>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
