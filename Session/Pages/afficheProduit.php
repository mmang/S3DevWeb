<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 panier, sessions" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, panier avec sessions" />
	<title>Navigation dans un catalogue</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Description de votre drogue</h2>
<p> <a href="afficheCat.php">Catalogue</a></p>
<?php

	require_once(RACINE_SITE . 'include/connexion.php');
	require_once(RACINE_SITE . 'include/gestionPanier.php');

	if (!empty($_REQUEST["Produit"])) {

		if (!empty($_POST["Quantite"]) && !empty($_POST["Prix"])) {
			ajoutePanier($_POST["Produit"],$_POST["Quantite"],$_POST["Prix"]);
		}
	}
	if (!isset($_SESSION[SESSION_TOTAL_PRIX])) {
		$_SESSION[SESSION_TOTAL_PRIX] = 0;
	}
?>
<p style="text-align:right;">
		Total : <?php echo $_SESSION[SESSION_TOTAL_PRIX]; ?> €
		<a href="affichePanier.php">panier</a></p>
	<hr />
</div>
<div id="centre">

<?php
	if (!empty($_REQUEST["Produit"])) {
		// affichage du produit
		$requete = 'SELECT id, nom, provenance, qualite, description, prix from produits where id = \'' . $_REQUEST["Produit"] . '\';';
		$resultat = mysqli_query($CONNEXION,$requete);
		if (!empty($resultat)) {
			$monProduit = mysqli_fetch_assoc($resultat);
			if (!empty($monProduit)) {
				echo '<ul>';
				echo '<li> Nom : ',$monProduit['nom'],'</li>';
				echo '<li> Provenance : ',$monProduit['provenance'],'</li>';
				echo '<li> Qualité : ',$monProduit['qualite'],'</li>';
				echo '<li> Description : ',$monProduit['description'],'</li>';
				echo '<li> Prix : ',$monProduit['prix'],' €/g</li>';
				echo '</ul>',"\n";

			// affichage du formulaire pour ajouter au panier
				echo '<form action="afficheProduit.php" method="post">
					<div style="text-align:right;">
					<input type="hidden" name="Prix" value="', $monProduit['prix'],'" />
					<input type="hidden" name="Produit" value="', $monProduit['id'],'" />
					<label>Quantité : <input type="text" name="Quantite"  size="3" value="1" /></label>
					<input type="submit" name="Ajouter" value="Ajouter au panier" />
					</div>
					</form>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($CONNEXION);
		}
	}

	mysqli_close($CONNEXION);
?>
</div>

<?php include(RACINE_SITE.'include/piedDePage.php');?>
