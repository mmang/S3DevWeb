<?php
	require_once('../init.php');
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Mathieu MANGEOT" />
	<meta name="keywords" content="src2 sysInfo1 panier, sessions" />
	<meta name="description" content="Cours de Syst&eacute;mes d'information 1,
		exemple de script PHP, panier avec sessions" />
	<title>Enregistrement utilisateur</title>
	<link rel="stylesheet" href="<?php echo RACINE_WEB;?>style/site.css" type="text/css" />
</head>
<body lang="fr" xml:lang="fr">
<div id="enTete">
	<h1>Bienvenue au palais de la dope !</h1>
	<h2>Enregistrement utilisateur</h2>
<p> <a href="afficheCat.php">Catalogue</a></p>
 <hr />
</div>

<div id="partieCentrale">
<?php

	require_once(RACINE_SITE . 'include/connexion.php');

	// Ajouter
	if (isset($_REQUEST["Enregistrer"]) &&
		!empty($_REQUEST["Nom"]) &&
		!empty($_REQUEST["Prenom"]) &&
		!empty($_REQUEST["Login"]) &&
		!empty($_REQUEST["Passe"]) &&
		!empty($_REQUEST["PasseVerif"]) &&
		!empty($_REQUEST["Courriel"]) &&
		!empty($_REQUEST["Question"]) &&
		!empty($_REQUEST["Reponse"])) {
		if (strcmp($_REQUEST["Passe"],$_REQUEST["PasseVerif"])==0) {
			$passCrypte = md5($_REQUEST["Passe"]);
			$requete = 'INSERT INTO utilisateurs (nom, prenom, login, passe, courriel, question, reponse) VALUES (\'' .
		 		$_REQUEST["Nom"] . "' , '" .
		 		$_REQUEST["Prenom"] . "' , '" .
		 		$_REQUEST["Login"] . "' , '" .
		 		$passCrypte . "' , '" .
		 		$_REQUEST["Courriel"] . "' , '" .
		 		$_REQUEST["Question"] . "' , '" .
		 		$_REQUEST["Reponse"] . "');";
			$resultat = mysqli_query($CONNEXION,$requete);
			if ($resultat) {
				echo '<p>', $_REQUEST["Prenom"], ' ', $_REQUEST["Nom"], ' login : ', $_REQUEST["Login"], ' est bien enregistré</p>';
				$_SESSION[SESSION_LOGIN] = $_REQUEST["Login"];
			}
			else {
				echo "Erreur dans l'exécution de la requête.<br/>\n";
				echo "Message de MySQL : ", mysqli_error($CONNEXION);
			}
		}
		else {
			echo '<p>Erreur dans la frappe du mot de passe !</p>';
		}
	}

	mysqli_close($CONNEXION);
?>
</div>

<?php include(RACINE_SITE.'include/piedDePage.php');?>
