<?php
	require_once('../init.php');
	include(RACINE_SITE.'include/entete.php');
?>
<section id="partieCentrale">
<?php

	require_once(RACINE_SITE . 'include/connexion.php');
	$categorie = 0;
	if (!empty($_REQUEST['Categorie'])) {
		$categorie = $_REQUEST['Categorie'];
	}

	// affichage du parent
	$requete = 'SELECT nomCat, parent from categories where idCat = \'' . $categorie . '\';';
	$resultat = mysqli_query ($CONNEXION,$requete);
	if (!empty($resultat)) {
		$maCat = mysqli_fetch_assoc ($resultat);
		if (!empty($maCat)) {
			echo '<p> <a href="afficheCat.php?Categorie=',$maCat['parent'],'">&lt;&lt;</a> <strong>',$maCat['nomCat'],'</strong></p>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}

	// affichage des sous-catégories
		echo '<ul>';
	$requete = 'SELECT nomCat, idCat from categories where parent = \'' . $categorie . '\';';
	$resultat = mysqli_query ($CONNEXION,$requete);
	if (!empty($resultat)) {
		while ($maCat = mysqli_fetch_assoc ($resultat)) {
			echo '<li> <a href="afficheCat.php?Categorie=',$maCat['idCat'],'">',$maCat['nomCat'],'</a></li>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}

	// affichage des produits
	$requete = 'SELECT nom, id from produits where idcategorie = \'' . $categorie . '\';';
	$resultat = mysqli_query ($CONNEXION,$requete);
	if (!empty($resultat)) {
		while ($monProduit = mysqli_fetch_assoc ($resultat)) {
			echo '<li> <a href="afficheProduit.php?Produit=',$monProduit['id'],'">',$monProduit['nom'],'</a></li>';
		}
	}
	else {
		echo "Erreur dans l'exécution de la requête.<br/>\n";
		echo "Message de MySQL : ", mysqli_error($CONNEXION);
	}
		echo '</ul>';

	mysqli_close($CONNEXION);
?>
</section>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
