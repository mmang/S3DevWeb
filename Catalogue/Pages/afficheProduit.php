<?php
	require_once('../init.php');
	include(RACINE_SITE.'include/entete.php');
?>
<section id="partieCentrale">
<?php

	require_once(RACINE_SITE . 'include/connexion.php');

	if (!empty($_REQUEST['Produit'])) {
		$requete = 'SELECT nom, provenance, qualite, description, prix from produits where id = \'' . $_REQUEST['Produit'] . '\';';
		$resultat = mysqli_query ($CONNEXION,$requete);
		if (!empty($resultat)) {
			$monProduit = mysqli_fetch_assoc($resultat);
			if (!empty($monProduit)) {
				echo '<ul>';
				echo '<li> Nom : ',$monProduit['nom'],'</li>';
				echo '<li> Provenance : ',$monProduit['provenance'],'</li>';
				echo '<li> Qualité : ',$monProduit['qualite'],'</li>';
				echo '<li> Description : ',$monProduit['description'],'</li>';
				echo '<li> Prix : ',$monProduit['prix'],' €/g</li>';
				echo '</ul>';
			}
		}
		else {
			echo "Erreur dans l'exécution de la requête.<br/>\n";
			echo "Message de MySQL : ", mysqli_error($connexion);
		}
	}

	mysqli_close($CONNEXION);
?>
</section>

<?php	include(RACINE_SITE.'include/piedDePage.php');?>
